import './App.scss';
import './components/slider/styles.css'
import { HeaderComponent } from './components/layouts/HeaderComponent';
import { FooterComponent } from './components/layouts/FooterComponent';
import { FeaturesComponent } from './components/features/FeaturesComponent';
import { GalleryComponent } from './components/gallery/GalleryComponent';
import { VideoComponent } from './components/video/VideoBlockComponent';
import { ContactComponent } from './components/contact/ContactBlockComponent';



function App() {
  return (
    <>
      <HeaderComponent/>
      <div className='main'>
        <FeaturesComponent/>
        <GalleryComponent/>
        <VideoComponent/>
        <ContactComponent/>
      </div>
      <FooterComponent/>
    </>
  );
}

export default App;
