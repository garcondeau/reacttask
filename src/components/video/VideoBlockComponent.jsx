import {ImPlay3} from 'react-icons/im'
import {ModalWindow} from './ModalWindow';

let isOpen = false

const toggleModalVideo = () => {
    if (!isOpen) {
        document.getElementById('modal-video').classList.add('modal-video--open');
        isOpen = true;
    } else {
        document.getElementById('modal-video').classList.remove('modal-video--open');
        isOpen = false;
    }
}

export const VideoComponent = () => {
    return (
        <div className="video">
            <div className="container video__container">
                <button className="video__play-btn" onClick={toggleModalVideo}>
                    <ImPlay3 size={'1.875rem'} color={'#fff'}/>
                </button>
                <div className="modal-video" id="modal-video" onClick={toggleModalVideo}>
                    <ModalWindow video={'https://www.youtube.com/embed/dQw4w9WgXcQ?controls=0'}/>
                </div>
                <h1 className="item-title video__item-title">Watch the best Technology in </h1>
                <h2 className="item-subtitle video__item-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis.
                    Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet.
                    Quisque non rhoncus sem.</h2>
            </div>
        </div>
    )
}