export const ModalWindow = (props) => {
    return (
        <div className="modal-video__inner">
            <iframe src={props.video} title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    )
}