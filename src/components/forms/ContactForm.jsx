export const ContactForm = () => {
    return(
        <form className="contact__form" action="#">
            <h1 className="form-title">Contact</h1>
            <input className="contact__form-input-name" placeholder="Your name" type="text"></input>
            <input className="contact__form-input-email" placeholder="Your email" type="email"></input>
            <input className="contact__form-input-subject" placeholder="Subject" type="text"></input>
            <textarea className="contact__form-input-message" placeholder="Message"></textarea>
            <button className="form__submit-btn">Send</button>
        </form>
    )
}