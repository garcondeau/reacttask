export const ContentItemComponent = (props) => {
    return (
        <div className="content-block__item">
            <div className="content-block__item-image">
                <img src={props.img}/>
                <a class="content-block__link-add-screenshot" href="#"></a>
            </div>
            <h1 className="content-block__item-title">{props.title}</h1>
            <h2 className="content-block__item-desc">{props.desc}</h2>
        </div>
    )
}
