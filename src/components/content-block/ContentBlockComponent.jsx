import { ContentItemComponent } from "./ContentItemComponent"

export const ContentBlockComponent = (props) => {

    return(
        <>
            <h1 className="content-block__title">{props.block_title}</h1>
            <h2 className="content-block__subtitle">{props.block_subtitle}</h2>
            <div className="content-block__container">
                {
                    props.block_items.map((item) => (<div className="column"><ContentItemComponent img={item.img} title={item.title} desc={item.desc}/></div>))
                }
            </div>
        </>
    )
}