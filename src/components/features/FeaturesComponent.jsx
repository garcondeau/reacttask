import {ContentBlockComponent} from "../content-block/ContentBlockComponent"
import img1 from '../../assets/images/features/01.png'
import img2 from '../../assets/images/features/02.png'
import img3 from '../../assets/images/features/03.png'
import img4 from '../../assets/images/features/04.png'


const FeaturesProps = {
    'block_title': 'summarise the features',
    'block_subtitle': 'summarise what your product is all about',
    'block_items': [
        {
            "img": img1,
            "title": "Attractive Layout",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img2,
            "title": "Fresh Design",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img3,
            "title": "multipurpose",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img4,
            "title": "Easy to customize",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        }
    ]
}


export const FeaturesComponent = () => {
    return (
        <div className="content-block">
            <div className="container features__container">
                <ContentBlockComponent block_title={FeaturesProps.block_title}
                                       block_subtitle={FeaturesProps.block_subtitle}
                                       block_items={FeaturesProps.block_items}/>
            </div>
        </div>
    )
}