import SliderComponent from "../slider/SliderComponent";
import { MenuComponent } from "../navs/MenuComponent";
import { LogoComponent } from "../navs/LogoComponent";

export const HeaderComponent = () => {
    return(
        <div className="header">
        <div className="header__top">
            <div className="container header__container">
                <LogoComponent/>
                <MenuComponent/>
            </div>
        </div>
        <div className="header__content">
            <SliderComponent/>
        </div>
    </div>
    )
}