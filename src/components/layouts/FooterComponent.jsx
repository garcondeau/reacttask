export const FooterComponent = () => {
    return (
        <div className="footer">
            <div className="container footer__container">
            <p>Copyright © 2013 | bazinger | All Rights Reserved</p>
            <div className="footer__links">
                <a href="#">Terms of Service</a>
                <a href="#">
                | Privacy Policy</a>
            </div>
            </div>
        </div>
    )
}