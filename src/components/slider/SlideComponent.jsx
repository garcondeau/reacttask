import iPhone_5s from '../../assets/images/carousel/iPhone_5S.png'
import { DiAndroid } from 'react-icons/di'
import {BsApple} from 'react-icons/bs'

export const SlideComponent = () => {
    return (
        <div className="slider__item">
            <img alt="" className="slider__img" src={iPhone_5s}></img>
            <div className="slider__item-text">
                <h1 className="item-title slider__item-title">Simple, Beautiful</h1>
                <h2 className="item-subtitle slider__item-subtitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae
                    tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean
                    ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id
                    laoreet. Quisque non rhoncus sem.
                </h2>
                <div className="slider__item-buttons">
                    <button>DOWNLOAD</button>
                    <button>LEARN MORE</button>
                </div>
                <div className="slider__downloads">
                    <h3>
                        Available on:
                    </h3>
                    <button>
                        <BsApple color={"white"} size={"1.625rem"}/>
                    </button>
                    <button>
                        <DiAndroid color={"white"} size={"1.625rem"}/>
                    </button>
                </div>
            </div>
        </div>
    )
}