import React from "react";
import { Carousel } from "react-responsive-carousel";
import { SlideComponent } from "./SlideComponent";

const SliderComponent = () => {
    return(
        <Carousel infiniteLoop showStatus={false} showThumbs={false}>
            <SlideComponent/>
            <SlideComponent/>
            <SlideComponent/>
        </Carousel>
    )
}
export default SliderComponent;
