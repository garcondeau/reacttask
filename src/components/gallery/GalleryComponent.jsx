import {ContentBlockComponent} from "../content-block/ContentBlockComponent"
import img1 from '../../assets/images/gallery/01.png'


const GalleryProps = {
    'block_title': 'summarise the features',
    'block_subtitle': 'summarise what your product is all about',
    'block_items': [
        {
            "img": img1,
            "title": "Attractive Layout",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img1,
            "title": "Fresh Design",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img1,
            "title": "multipurpose",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        },
        {
            "img": img1,
            "title": "Easy to customize",
            "desc": "Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar."
        }
    ]
}


export const GalleryComponent = () => {
    return (
        <div className="gallery">
            <div className="content-block">
                <div className="container features__container">
                    <ContentBlockComponent block_title={GalleryProps.block_title}
                                           block_subtitle={GalleryProps.block_subtitle}
                                           block_items={GalleryProps.block_items}/>
                </div>
            </div>
        </div>
    )
}