const MenuItems = [
    'FEATURES',
    'GALLERY',
    'VIDEO',
    'PRICES',
    'TESTIMONIALS',
    'DOWNLOAD',
    'CONTACT'
]

let isOpen = false

const toggleDrawer = () => {
    if (!isOpen) {
        document.getElementById('menu').classList.add('menu--open');
        document.getElementById('menu__drawer').classList.add('open');
        isOpen = true;
    } else {
        document.getElementById('menu').classList.remove('menu--open');
        document.getElementById('menu__drawer').classList.remove('open');
        isOpen = false;
    }
}

export const MenuComponent = () => {
    return (
        <>
            <nav className="menu" id="menu">
                <ul className="menu__list">
                    <li className="menu__item menu__item-active">
                        <a className="menu__link" href="#">HOME</a>
                    </li>
                    {
                        MenuItems.map((item) => <li className="menu__item"><a className="menu__link" href="#">{item}</a>
                            </li>
                        )
                    }
                </ul>
            </nav>
            < button className="menu__drawer" id="menu__drawer" onClick={toggleDrawer}>
                <span></span>
                <span></span>
                <span></span>
            </button>
        </>
    )
}