import signal from '../../assets/images/logo/signal.png'

export const LogoComponent = () => {
    return(
        <a className="logo header__logo" href="#">
            <img alt="logo" className="logo__img" src={signal}></img>
            <div className="logo__text">zinger</div>
        </a>
    )
}