import {ContactForm} from "../forms/ContactForm"

export const ContactComponent = () => {
    return (
        <div className="contact">
            <div className="container contact__container">
                <ContactForm/>
            </div>
        </div>
    )
}